using System;
using System.Collections.Generic;
using TimeEventStoreBack.Models;

namespace TimeEventStoreBack.Dtos
{
    public partial class ProductsCampaignDto
    {
        public Campaign Campaign { get; set; }
        public List<Product> Products { get; set; }
    }
}