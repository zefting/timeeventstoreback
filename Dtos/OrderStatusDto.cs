﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TimeEventStoreBack.Dtos
{
    public partial class OrderStatusDto
    {
        public int OrderId { get; set; }
        public bool? Shipped { get; set; }
    }
}
