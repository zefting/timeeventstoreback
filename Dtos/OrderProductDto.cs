﻿using System;
using System.Collections.Generic;
using TimeEventStoreBack.Models;

#nullable disable

namespace TimeEventStoreBack.Dtos
{
    public partial class OrderProductDto
    {
        public int OrderProductId { get; set; }
        public int OrderId { get; set; }
        public int ProductId { get; set; }

        public virtual Order Order { get; set; }
        public virtual Product OrderNavigation { get; set; }
    }
}
