﻿using System;
using System.Collections.Generic;
using TimeEventStoreBack.Models;

#nullable disable

namespace TimeEventStoreBack.Dtos
{
    public partial class CostumerDto
    {
        public int CostumerId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Address { get; set; }
        public int Zipcode { get; set; }
        public string City { get; set; }
        public string Email { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}
