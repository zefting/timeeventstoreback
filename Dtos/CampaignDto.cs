﻿using System;
using System.Collections.Generic;
using TimeEventStoreBack.Models;
#nullable disable

namespace TimeEventStoreBack.Dtos
{
    public partial class CampaignDto
    {
       

        public int CampaignId { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int CampaginSerie { get; set; }


    }
}
