﻿using System;
using System.Collections.Generic;
using TimeEventStoreBack.Models;

#nullable disable

namespace TimeEventStoreBack.Dtos
{
    public partial class OrderReadDto
    {

        public int OrderId { get; set; }
        public DateTime SoldDateTime { get; set; }

        public float OrderTotal { get; set; }

        public virtual Campaign Campaign { get; set; }
        public virtual Costumer Costumer { get; set; }
        
    }
}
