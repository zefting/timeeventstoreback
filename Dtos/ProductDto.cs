﻿using System;
using System.Collections.Generic;
using TimeEventStoreBack.Models;

#nullable disable

namespace TimeEventStoreBack.Dtos
{
    public partial class ProductDto
    {
          public string Name { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public int CampaignId { get; set; }
        public string Thumbnail { get; set; }
        public string ProductImage { get; set; }
        public virtual CampaignDto Campaign { get; set; }
        public virtual ICollection<OrderProduct> OrderProducts { get; set; }
    }
}
