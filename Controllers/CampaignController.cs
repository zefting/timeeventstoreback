﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TimeEventStoreBack.Models;
using TimeEventStoreBack.Data;
using TimeEventStoreBack.Dtos;
namespace TimeEventStoreBack.Controllers
{
    [ApiController]
    [Route("api/products")]
    public class CampaignController : ControllerBase
    {
      private readonly ITimedEventRepo _repository;

      public CampaignController(ITimedEventRepo repository)
      {
         _repository = repository;
      }

      [HttpGet]
      [Route("productsCampaign/{campaignId}")]
      public ActionResult<ProductsCampaignDto> GetProductsCampaignById(int campaignId)
      {
         var dto = new ProductsCampaignDto();
         dto.Campaign = _repository.GetCampaignById(campaignId);
         dto.Products = _repository.GetProducts(campaignId);
     
         return Ok(dto);

      }
      [HttpGet]
      [Route("campaignOverview/{campaignSeries}")]
      public ActionResult CampaignOverview(int campaignSeries)
      {  
         var campaignSeriesItems = _repository.GetCampaignSeries(campaignSeries);
         return Ok(campaignSeriesItems);
          
      }
   }
}