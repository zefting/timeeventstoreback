using System;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Microsoft.AspNetCore.Http;
using TimeEventStoreBack.Data;
using TimeEventStoreBack.Models;
using TimeEventStoreBack.Dtos;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;

namespace TimeEventStoreBack.Controllers
{
    [Route("api")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly ITimedEventRepo _repository;
         public ProductController(ITimedEventRepo repository)
        {
            _repository = repository;
        }
        [Route("createProduct")]
        [HttpPost,DisableRequestSizeLimit]
        public ActionResult<ProductReadDto> CreateProduct(ProductDto productDto)
        {
            
            var productModel = new Product();
            productModel.Name = productDto.Name;
            productModel.Price = productDto.Price;
            productModel.Description = productDto.Description;
            productModel.CampaignId = productDto.CampaignId;
            // save a product image and a thumbnail
                string jpgType = ".jpg";
                string pgnType = ".png";
                var file = Request.Form.Files[0];
                string tempfolderName = Path.Combine("StaticFiles", "Uploads","temp");
                string folderName = Path.Combine("StaticFiles", "Uploads");
                string jpgFolderName = Path.Combine("StaticFiles", "Uploads", "jpg");
                string pgnFolderName = Path.Combine("StaticFiles", "Uploads", "png");
                string original = "orginal";
                string size256x256 = "256x256";
                string size1920x1080  = "1920x1080";
                string tempPathToSave = Path.Combine(Directory.GetCurrentDirectory(), tempfolderName);
                if(file.Length > 0)
                {
                    string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim().ToString();
                 
                    var newFileName = Guid.NewGuid();
                    
                    string tempfullPath = Path.Combine(tempfolderName, fileName);
                    
                    string dbPath = Path.Combine(Directory.GetCurrentDirectory(), newFileName.ToString());
                    using (var stream = new FileStream(tempfullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                    // check if the file was created, and if so get the file type and move it to the right folder.
                    if(System.IO.File.Exists(tempfullPath))
                    {
                        // get filetype
                        var fileType = Path.GetExtension(tempfullPath);
                        // if jpg, move it to the jpg folder, and if succes, delete the temp file.
                        if(fileType == jpgType)
                        {
                          string orgiPath = Path.Combine(jpgFolderName, newFileName + jpgType);
                            System.IO.File.Move(tempfullPath, Path.Combine(jpgFolderName, newFileName + jpgType));
                            if(System.IO.File.Exists(orgiPath))
                            {
                                
                                System.IO.File.Copy(orgiPath, Path.Combine(jpgFolderName, original, newFileName + jpgType), true);
                                var saveName = new FileUpload();
                                saveName.OldFileName = fileName;                          
                                saveName.NewFileName = Path.Combine(newFileName + jpgType);
                                _repository.SaveFileName(saveName);
                                _repository.SaveChanges();

                                // 256
                                string path256 = Path.Combine(jpgFolderName, size256x256, newFileName +"size256X256"+ jpgType);
                                System.IO.File.Copy(orgiPath, path256, true);
                                using var image256 = Image.Load(path256);
                                image256.Mutate(x => x.Resize(256, 256));
                                image256.Save(path256);

                                var saveName256 = new FileUpload();
                                saveName256.OldFileName = fileName;                          
                                saveName256.NewFileName = Path.Combine(newFileName +"size256X256"+ jpgType);
                                
                                productModel.Thumbnail = saveName256.NewFileName;
                                _repository.SaveFileName(saveName256);
                                _repository.SaveChanges();

                              
                                // 1920
                                string path1920 = Path.Combine(jpgFolderName, size1920x1080, newFileName +"size1920x1080"+ jpgType);
                                System.IO.File.Copy(orgiPath, path1920, true);
                                using var image1920 = Image.Load(path1920);
                                image1920.Mutate(x => x.Resize(1920, 1080));
                                image1920.Save(path1920);
                                var saveName1920 = new FileUpload();
                                saveName1920.OldFileName = fileName;                          
                                saveName1920.NewFileName = Path.Combine(newFileName +"size1920x1080"+ jpgType);
                                productModel.ProductImage = saveName1920.NewFileName;
                                _repository.SaveFileName(saveName1920);
                                _repository.SaveChanges();

                            }
                            System.IO.File.Delete(orgiPath);
                        }
                        // if pgn, move it to the jpg folder, and if succes, delete the temp file.
                        if(fileType == pgnType)
                        {
                            string orgiPath = Path.Combine(pgnFolderName, newFileName +  pgnType);
                            System.IO.File.Move(tempfullPath, Path.Combine(pgnFolderName, newFileName + pgnType));
                            
                            if(System.IO.File.Exists(orgiPath))
                            {
                                
                                System.IO.File.Copy(orgiPath, Path.Combine(pgnFolderName, original, newFileName + pgnType), true);
                                var saveName = new FileUpload();
                                saveName.OldFileName = fileName;                          
                                saveName.NewFileName = Path.Combine(newFileName + pgnType);
                                _repository.SaveFileName(saveName);
                                _repository.SaveChanges();

                                // 256
                                string path256 = Path.Combine(pgnFolderName, size256x256, newFileName +"size256X256"+ pgnType);
                                System.IO.File.Copy(orgiPath, path256, true);
                                using var image256 = Image.Load(path256);
                                image256.Mutate(x => x.Resize(256, 256));
                                image256.Save(path256);

                                var saveName256 = new FileUpload();
                                saveName256.OldFileName = fileName;                          
                                saveName256.NewFileName = Path.Combine(newFileName +"size256X256"+ pgnType);
                                productModel.Thumbnail = saveName256.NewFileName;
                                _repository.SaveFileName(saveName256);
                                _repository.SaveChanges();

                               

                                var saveName680 = new FileUpload();
                                saveName680.OldFileName = fileName;                          
                                saveName680.NewFileName = Path.Combine(newFileName +"size680x680"+ pgnType);
                                _repository.SaveFileName(saveName680);
                                _repository.SaveChanges();

                                // 1920
                                string path1920 = Path.Combine(pgnFolderName, size1920x1080, newFileName +"size1920x1080"+ pgnType);
                                System.IO.File.Copy(orgiPath, path1920, true);
                                using var image1920 = Image.Load(path1920);
                                image1920.Mutate(x => x.Resize(1920, 1080));
                                image1920.Save(path1920);
                                var saveName1920 = new FileUpload();
                                saveName1920.OldFileName = fileName;                          
                                saveName1920.NewFileName = Path.Combine(newFileName +"size1920x1080"+ pgnType);
                                productModel.ProductImage = saveName1920.NewFileName;
                                _repository.SaveFileName(saveName1920);
                                _repository.SaveChanges();
                                System.IO.File.Delete(orgiPath);
                            
                            }
                        
                        }
                    }
                }
            
            _repository.SaveProduct(productModel);
            _repository.SaveChanges();

            var returnItem = new ProductReadDto();
            returnItem.CampaignId = productModel.CampaignId;
            returnItem.Description = productModel.Description;
            returnItem.Name = productModel.Name;
            returnItem.Price = productModel.Price;
            returnItem.ProductId = productModel.ProductId;
            returnItem.ProductImage = productModel.ProductImage;
            returnItem.Thumbnail = productModel.Thumbnail;
            return returnItem;

            
        }
    }
}