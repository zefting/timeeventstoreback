﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TimeEventStoreBack.Models;
using TimeEventStoreBack.Data;
using TimeEventStoreBack.Dtos;
namespace TimeEventStoreBack.Controllers
{
   [ApiController]
   [Route("api/orders")]
   public class OrderController : ControllerBase
   {
      private readonly ITimedEventRepo _repository;

      public OrderController(ITimedEventRepo repository)
      {
         _repository = repository;
      }
      [HttpGet]
      [Route("{orderId}")]
      public ActionResult<OrderReadDto> GetOrderByOrderId (int orderId)
      {
         var orderDto = new OrderReadDto();
         var orderItem = _repository.GetOrderById(orderId);
         var costumerItem = _repository.GetCostumerById(orderItem.CostumerId);
         var campaignItem = _repository.GetCampaignById(orderItem.CampaignId); 
         if(orderItem != null)
         {
            orderDto.OrderId = orderItem.OrderId;
            orderDto.OrderTotal = orderItem.OrderTotal;
            orderDto.SoldDateTime = orderItem.SoldDateTime;
            if(campaignItem != null)
            {
               orderDto.Campaign = campaignItem;
            }
            if(costumerItem != null)
            {
               orderDto.Costumer = costumerItem;
            }
            return orderDto;
         }
         
         return NotFound();
      }

      [HttpGet]
      [Route("orderstatus/{orderId}")]
      public ActionResult<OrderStatusDto> GetOrderStatusByOrder (int orderId)
      {
         var orderStatus = new OrderStatusDto();
         var orderStatusItem = _repository.GetOrderStatusByOrder(orderId);
         if(orderStatusItem != null)
         {
            orderStatus.OrderId = orderStatusItem.OrderId;
            orderStatus.Shipped = orderStatusItem.Shipped;
            return orderStatus;
         }
         return NotFound();
      }
      [HttpPost]
      [Route("Placeorder")]
      public ActionResult<OrderReadDto> PlaceOrder(OrderDto orderDto)
      {
         // first store the costumer
         var costumerModel = new Costumer();
         costumerModel.Address = orderDto.Costumer.Address;
         costumerModel.City = orderDto.Costumer.City;
         costumerModel.Email = orderDto.Costumer.Email;
         costumerModel.Firstname = orderDto.Costumer.Firstname;
         costumerModel.Lastname = orderDto.Costumer.Lastname;
         costumerModel.Zipcode = orderDto.Costumer.Zipcode;
         _repository.SaveCostumer(costumerModel);
         _repository.SaveChanges();
         // save orderdetales
         // get the new costumer id 
         var costumer = _repository.GetCostumerById(orderDto.Campaign.CampaignId);
         var orderModel = new Order();
         orderModel.OrderTotal = orderDto.OrderTotal;
         orderModel.CampaignId = orderDto.Campaign.CampaignId;
         orderModel.CostumerId = costumer.CostumerId;
         _repository.PlaceOrder(orderModel);
         _repository.SaveChanges();

         // save the orderproduct relation
            var orderProductList = new List<OrderProduct>();
            foreach(var productId in orderDto.ProductIds)
            {
                var ordersProduct = new OrderProduct();

                ordersProduct.OrderId = orderModel.OrderId;
                ordersProduct.ProductId = productId;

                orderProductList.Add(ordersProduct);
            }
            _repository.PlaceOrderProductRelation(orderProductList);
            _repository.SaveChanges();

            // return an object representing the new order.
            var returnItem = new OrderReadDto();
            returnItem.OrderId = orderModel.OrderId;
            returnItem.OrderTotal = orderModel.OrderTotal;
            returnItem.SoldDateTime = orderModel.SoldDateTime;
            returnItem.Campaign = orderModel.Campaign;
            returnItem.Costumer = costumer;
            
         return returnItem;
      }
   }
}