﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TimeEventStoreBack.Models
{
    public partial class FileUpload
    {
        public int FileId { get; set; }
        public string OldFileName { get; set; }
        public string NewFileName { get; set; }
    }
}
