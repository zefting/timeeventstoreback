﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TimeEventStoreBack.Models
{
    public partial class Order
    {
        public Order()
        {
            OrderProducts = new HashSet<OrderProduct>();
        }

        public int OrderId { get; set; }
        public int CostumerId { get; set; }
        public DateTime SoldDateTime { get; set; }
        public int CampaignId { get; set; }
        public float OrderTotal { get; set; }

        public virtual Campaign Campaign { get; set; }
        public virtual Costumer Costumer { get; set; }
        public virtual ICollection<OrderProduct> OrderProducts { get; set; }
    }
}
