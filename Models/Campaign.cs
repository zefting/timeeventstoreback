﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TimeEventStoreBack.Models
{
    public partial class Campaign
    {
        public Campaign()
        {
            Orders = new HashSet<Order>();
            Products = new HashSet<Product>();
        }

        public int CampaignId { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int CampaginSerie { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
