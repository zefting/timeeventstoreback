﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TimeEventStoreBack.Models
{
    public partial class OrderStatus
    {
        public int StatusId { get; set; }
        public int OrderId { get; set; }
        public bool? Shipped { get; set; }
    }
}
