﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TimeEventStoreBack.Models
{
    public partial class Product
    {
        public Product()
        {
            OrderProducts = new HashSet<OrderProduct>();
        }

        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public int CampaignId { get; set; }
        public string Thumbnail { get; set; }
        public string ProductImage { get; set; }

        public virtual Campaign Campaign { get; set; }
        public virtual ICollection<OrderProduct> OrderProducts { get; set; }
    }
}
