﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace TimeEventStoreBack.Models
{
    public partial class TimeEventStore1Context : DbContext
    {
        public TimeEventStore1Context()
        {
        }

        public TimeEventStore1Context(DbContextOptions<TimeEventStore1Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Campaign> Campaigns { get; set; }
        public virtual DbSet<Costumer> Costumers { get; set; }
        public virtual DbSet<FileUpload> FileUploads { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderProduct> OrderProducts { get; set; }
        public virtual DbSet<OrderStatus> OrderStatuses { get; set; }
        public virtual DbSet<Product> Products { get; set; }

       
    

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Campaign>(entity =>
            {
                entity.ToTable("campaigns");

                entity.Property(e => e.CampaignId)
                    .HasColumnType("int(11)")
                    .HasColumnName("campaignId");

                entity.Property(e => e.CampaginSerie)
                    .HasColumnType("int(11)")
                    .HasColumnName("campaginSerie");

                entity.Property(e => e.End)
                    .HasColumnType("datetime")
                    .HasColumnName("end");

                entity.Property(e => e.Start)
                    .HasColumnType("datetime")
                    .HasColumnName("start");
            });

            modelBuilder.Entity<Costumer>(entity =>
            {
                entity.ToTable("costumers");

                entity.Property(e => e.CostumerId)
                    .HasColumnType("int(11)")
                    .HasColumnName("costumerId");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasColumnType("varchar(500)")
                    .HasColumnName("address")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasColumnType("varchar(500)")
                    .HasColumnName("city")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnType("varchar(500)")
                    .HasColumnName("email")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Firstname)
                    .IsRequired()
                    .HasColumnType("varchar(100)")
                    .HasColumnName("firstname")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Lastname)
                    .IsRequired()
                    .HasColumnType("varchar(250)")
                    .HasColumnName("lastname")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Zipcode)
                    .HasColumnType("int(11)")
                    .HasColumnName("zipcode");
            });

            modelBuilder.Entity<FileUpload>(entity =>
            {
                entity.HasKey(e => e.FileId)
                    .HasName("PRIMARY");

                entity.ToTable("FileUpload");

                entity.Property(e => e.FileId).HasColumnType("int(11)");

                entity.Property(e => e.NewFileName)
                    .IsRequired()
                    .HasColumnType("varchar(1000)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.OldFileName)
                    .IsRequired()
                    .HasColumnType("varchar(1000)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.ToTable("orders");

                entity.HasIndex(e => e.CampaignId, "campaign");

                entity.HasIndex(e => e.CostumerId, "costumer");

                entity.Property(e => e.OrderId)
                    .HasColumnType("int(11)")
                    .HasColumnName("orderId");

                entity.Property(e => e.CampaignId)
                    .HasColumnType("int(11)")
                    .HasColumnName("campaignId");

                entity.Property(e => e.CostumerId)
                    .HasColumnType("int(11)")
                    .HasColumnName("costumerId");

                entity.Property(e => e.OrderTotal).HasColumnName("orderTotal");

                entity.Property(e => e.SoldDateTime)
                    .HasColumnType("datetime")
                    .HasColumnName("soldDateTime");

                entity.HasOne(d => d.Campaign)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.CampaignId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("campaign");

                entity.HasOne(d => d.Costumer)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.CostumerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("costumer");
            });

            modelBuilder.Entity<OrderProduct>(entity =>
            {
                entity.ToTable("orderProducts");

                entity.HasIndex(e => e.OrderId, "product");

                entity.Property(e => e.OrderProductId)
                    .HasColumnType("int(11)")
                    .HasColumnName("orderProductId");

                entity.Property(e => e.OrderId)
                    .HasColumnType("int(11)")
                    .HasColumnName("orderId");

                entity.Property(e => e.ProductId)
                    .HasColumnType("int(11)")
                    .HasColumnName("productId");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.OrderProducts)
                    .HasForeignKey(d => d.OrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("order");

                entity.HasOne(d => d.OrderNavigation)
                    .WithMany(p => p.OrderProducts)
                    .HasForeignKey(d => d.OrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("product");
            });

            modelBuilder.Entity<OrderStatus>(entity =>
            {
                entity.HasKey(e => e.StatusId)
                    .HasName("PRIMARY");

                entity.ToTable("orderStatus");

                entity.Property(e => e.StatusId)
                    .HasColumnType("int(11)")
                    .HasColumnName("statusId");

                entity.Property(e => e.OrderId)
                    .HasColumnType("int(11)")
                    .HasColumnName("orderId");

                entity.Property(e => e.Shipped).HasColumnName("shipped");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("products");

                entity.HasIndex(e => e.CampaignId, "campaignId");

                entity.Property(e => e.ProductId)
                    .HasColumnType("int(11)")
                    .HasColumnName("productId");

                entity.Property(e => e.CampaignId)
                    .HasColumnType("int(11)")
                    .HasColumnName("campaignId");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnType("varchar(500)")
                    .HasColumnName("description")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(100)")
                    .HasColumnName("name")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Price)
                    .HasColumnType("int(11)")
                    .HasColumnName("price");

                entity.Property(e => e.ProductImage)
                    .IsRequired()
                    .HasColumnType("varchar(1000)")
                    .HasColumnName("productImage")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Thumbnail)
                    .IsRequired()
                    .HasColumnType("varchar(1000)")
                    .HasColumnName("thumbnail")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.HasOne(d => d.Campaign)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.CampaignId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("campaignId");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
