using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TimeEventStoreBack.Models;
using Pomelo.EntityFrameworkCore.MySql;


namespace TimeEventStoreBack.Data
{
    public interface ITimedEventRepo
    {
         // save to database
        bool SaveChanges();

        // get campagin by id 
        Campaign GetCampaignById(int CampaignId);
        List<Campaign> GetCampaignSeries(int CampaignSerie);

        //products 
        List<Product> GetProducts(int CampaignId);
        void SaveProduct(Product product);

        //orders
        List<Order> GetAllOrdersByCostumer(int costumerId);
        Order GetOrderById(int orderId);
        void PlaceOrderProductRelation(List<OrderProduct> orderProductList);
        void PlaceOrder(Order orderModel);

        // Get orderStatus by Order
        OrderStatus GetOrderStatusByOrder (int orderId);

        //costumer
        Costumer GetCostumerById(int costumerId);
        void SaveCostumer(Costumer costumerModel);

        void SaveFileName(FileUpload fileUpload);

        FileUpload GetFileByName(string getFileName);
      
    }
}