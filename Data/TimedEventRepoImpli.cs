using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using TimeEventStoreBack.Models;
using Pomelo.EntityFrameworkCore.MySql;

namespace TimeEventStoreBack.Data
{
    public class TimeEventStoreRepoImpli : ITimedEventRepo
    {
        private readonly TimeEventStore1Context _context;
        public TimeEventStoreRepoImpli(TimeEventStore1Context context)
        {
            _context = context;
        }
        // Save to the database 
        public bool SaveChanges()
        {
            return(_context.SaveChanges() >=0);
        }
        public Campaign GetCampaignById(int CampaignId)
        {
            return _context.Campaigns.FirstOrDefault(campaign => campaign.CampaignId == CampaignId);
        }
        public List<Campaign> GetCampaignSeries(int CampaignSerie)
        {
            return _context.Campaigns.Where(campaignSerie => campaignSerie.CampaginSerie == CampaignSerie).ToList();
        }

        public List<Product> GetProducts(int CampaignId)
        {
            return _context.Products.Where(product => product.CampaignId == CampaignId).ToList();
        }
         public void SaveProduct(Product product)
        {
            if(product == null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            _context.Products.Add(product);
        }
         //orders
        public List<Order> GetAllOrdersByCostumer(int costumerId)
        {
            return _context.Orders.Where(order => order.CostumerId == costumerId).ToList();
        }
        public Order GetOrderById(int orderId)
        {
            return _context.Orders.FirstOrDefault(order => order.OrderId == orderId);
        }
         public void PlaceOrderProductRelation(List<OrderProduct> orderProductList)
        {
            if(orderProductList == null)
            {
                throw new ArgumentNullException(nameof(orderProductList));
            }
            _context.OrderProducts.AddRange(orderProductList);
        }
        public void PlaceOrder(Order orderModel)
        {
            if(orderModel == null)
            {
                throw new ArgumentNullException(nameof(orderModel));
            }
            _context.Orders.Add(orderModel);

        }
        // Get orderStatus by Order
        public OrderStatus GetOrderStatusByOrder (int orderId)
        {
            return _context.OrderStatuses.FirstOrDefault(orderstatus => orderstatus.OrderId == orderId);
        }

        //costumer
        public Costumer GetCostumerById(int costumerId)
        {
            return _context.Costumers.FirstOrDefault(costumer => costumer.CostumerId == costumerId);
        }
         public void SaveCostumer(Costumer costumerModel)
        {
            if(costumerModel == null)
            {
                throw new ArgumentNullException(nameof(costumerModel));
            }
             _context.Costumers.Add(costumerModel);
        }
        public void SaveFileName(FileUpload fileUpload)
        {
            if(fileUpload == null)
            {
                throw new ArgumentNullException(nameof(fileUpload));
            }
            _context.FileUploads.Add(fileUpload);
        }
        public FileUpload GetFileByName(string getFileName)
        {
            return _context.FileUploads.FirstOrDefault(p => p.NewFileName == getFileName);
        }
    }
}